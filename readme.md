Using esp-open-rtos instead of ESP8266_RTOS_SDK

Reasoning is:

According to esp-open-rtos folks:

* licensing for ESP8266_RTOS_SDK is tricky
* TLS drivers for esp-open-rtos are better both from a licensing and functionality point of view
* ESP8266_RTOS_SDK tends to lag more behind FreeRTOS updates

# For building esp-open-sdk, as recommended by esp-open-rtos:

make toolchain esptool libhal STANDALONE=n

Mac notes for case sensitivity:

hdiutil create -type SPARSE -fs 'Case-sensitive HFS+' -size 10g -volname workspace case-sensitive
hdiutil attach -mountpoint ~/myMountPoint case-sensitive

Above is doable, but new preferred way is to use docker image to do all builds.  It can be 
tricky to run esptool from within docker itself, so if that's the case, install esptool 
on the host:

pip install esptool

A convenient docker image is:

malachib/esp-open-rtos:latest-mal-lwip
