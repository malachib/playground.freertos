#/bin/bash

# according to this http://stackoverflow.com/questions/34011743/does-openssl-supports-ecdhe-ecdsa-aes128-ccm8-for-dtls1-2-or-tls1-2
# it's implied to use secp521r1 for CCM8.  But only implied

#CURVE=secp256k1
CURVE=secp521r1
KEYFILE=key.pem
#HASH=-AES128-CCM8
HASH=-sha256
#HASH=-mac
CONF=defaults.conf

openssl ecparam -genkey -name $CURVE -out $KEYFILE
openssl req -new $HASH -key $KEYFILE -out csr.csr -config $CONF
openssl req -x509 $HASH -days 365 -key $KEYFILE -in csr.csr -out certificate.pem -config $CONF
#openssl req -in csr.csr -text -noout
#| grep -i "Signature.*SHA256" && echo "All is well" || echo "This certificate will stop working in 2017! You must update OpenSSL to generate a widely-compatible certificate"
