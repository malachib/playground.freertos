#!/bin/bash

CONF=defaults.conf

openssl ecparam -name secp521r1 -out my-ca-key.pem -genkey
openssl req -new -x509 -days 365 -key my-ca-key.pem -out my-ca-cert.pem -config $CONF

openssl ecparam -name secp521r1 -out my-server-key.pem -genkey
openssl req -new -key my-server-key.pem -out my-server-csr.pem -config $CONF
openssl x509 -req -days 365 -in my-server-csr.pem -CA my-ca-cert.pem -CAkey my-ca-key.pem -set_serial 01 -out my-server-cert.pem

