#include "espressif/esp_common.h"
#include "FreeRTOS.h"
#include "task.h"
#include <esp/uart.h>

#include <string.h>
#include <stdio.h>

#include "lwip/api.h"
#include "lwip/udp.h"
#include "lwip/pbuf.h"

const int gpio = 2;


void blinkenTask(void *pvParameters)
{
    int counter = 0;

    gpio_enable(gpio, GPIO_OUTPUT);
    while(1) {
        gpio_write(gpio, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_write(gpio, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        printf("Blinking: %d\r\n", counter++);
    }
}

void debugPrintBuffer(const char *title, char* buffer, size_t len)
{
    printf(title);
    while(len--) printf(" %x", (uint16_t)(*buffer++));
    puts("");
}

void udpTask(void *pvParameters)
{
    struct ip_info local_ip;

    const uint8_t mode = sdk_wifi_get_opmode();

    if (mode & STATION_MODE) {
      sdk_wifi_get_ip_info(STATION_IF, &local_ip);
    } else {
      sdk_wifi_get_ip_info(SOFTAP_IF, &local_ip);
    }

    for(;;)
    {
        struct netconn *conn = netconn_new_with_proto_and_callback(NETCONN_UDP, 0, NULL);
        char buf[512];

        if(!conn)
        {
            printf("Failed to allocate socket\n");
            abort();
        }

        netconn_bind(conn, IP_ADDR_ANY, 5000);
        netconn_set_recvtimeout(conn, 0); // blocking

        struct netbuf* nbFrom = netbuf_new();
        netconn_recv(conn, &nbFrom);

        void* _recvBuf;
        u16_t recvLen = 0;
        const size_t offset = 50;

        err_t err = netbuf_data(nbFrom, &_recvBuf, &recvLen);

        printf("Status: %d\n", (uint16_t)err);
        printf("Got %d bytes\n", recvLen);
        debugPrintBuffer("", (char*)_recvBuf, recvLen);

        memset(buf, 0, sizeof(buf));
        char* offsetbuf = buf + offset;
        sprintf(offsetbuf, "Test send! (got: %d bytes)", recvLen);
        size_t buflen = strlen(offsetbuf);

        struct netbuf* nb = netbuf_new();
        // If using a regular netbuf_alloc, no buffer issues occur
        netbuf_ref(nb, offsetbuf, buflen);

        printf("About to send\n");

        // buffer shwon (first 50 bytes) is all zeroes
        debugPrintBuffer("buffer pre offset:", (char*)buf, offset);

        // Buffer underrun appears to occur here
        netconn_sendto(conn, nb, netbuf_fromaddr(nbFrom), 5001);

        // observe not all zeroes as one would expect
        debugPrintBuffer("buffer pre offset:", (char*)buf, offset);

        netbuf_delete(nb);
        netbuf_delete(nbFrom);
        netconn_disconnect(conn);
        netconn_delete(conn);
    }
}


void user_init(void)
{
  uart_set_baud(0, 115200);

  xTaskCreate(blinkenTask, "blinkenTask", 256, NULL, 2, NULL);
  xTaskCreate(udpTask, "udpTask", 4096, NULL, 2, NULL);
}
