#define LWIP_IGMP 1
#define LWIP_MDNS_RESPONDER 1

// mDNS needs this
// Enabling this does similar things as enabling LWIP_IGMP , basically breaks offset
// for hwaddr
#define LWIP_NUM_NETIF_CLIENT_DATA 2

#define MDNS_MAX_SERVICES 3

#include_next <lwipopts.h>

#undef TCPIP_THREAD_STACKSIZE
#define TCPIP_THREAD_STACKSIZE  768
