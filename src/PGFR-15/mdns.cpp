#include <lwip/apps/mdns.hpp>

void mdns_task(void *pvParameters)
{
    lwip::mdns::Responder mDNS("mdns-test");

    mDNS += lwip::mdns::http();

    for(;;);
}
