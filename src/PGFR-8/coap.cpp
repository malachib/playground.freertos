#include <stdlib.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "esp8266.h"

#include <string.h>

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"
#include "lwip/api.h"
#include "lwip/udp.h"
#include "lwip/igmp.h"

extern "C" {
#include "coap.h"
}

extern "C" {
extern coap_resource_t resources[];    
void resource_setup(const coap_resource_t *resources);

/*
static int handle_get_well_known_core(const coap_resource_t *resource,
                                      const coap_packet_t *inpkt,
                                      coap_packet_t *pkt); */
}


void coapTask(void *pvParameters)
{
    int fd;
#ifdef IPV6
    struct sockaddr_in6 servaddr, cliaddr;
#else /* IPV6 */
    struct sockaddr_in servaddr, cliaddr;
#endif /* IPV6 */
    uint8_t buf[1024];

#ifdef IPV6
    fd = socket(AF_INET6,SOCK_DGRAM,0);
#else /* IPV6 */
    fd = socket(AF_INET,SOCK_DGRAM,0);
#endif /* IPV6 */

    memset(&servaddr,0,sizeof(servaddr));
#ifdef IPV6
    servaddr.sin6_family = AF_INET6;
    servaddr.sin6_addr = in6addr_any;
    servaddr.sin6_port = htons(COAP_DEFAULT_PORT);
#else /* IPV6 */
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(COAP_DEFAULT_PORT);
#endif /* IPV6 */
    //UDP_Multicast_init();

    bind(fd,(struct sockaddr *)&servaddr, sizeof(servaddr));
    
    
    resource_setup(resources);

    while(1)
    {
        int n, rc;
        socklen_t len = sizeof(cliaddr);
        coap_packet_t pkt;

        n = recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&cliaddr, &len);
#ifdef YACOAP_DEBUG
        printf("Received: ");
        coap_dump(buf, n, true);
        printf("\n");
#endif

        if ((rc = coap_parse(buf, n, &pkt)) > COAP_ERR)
            printf("Bad packet rc=%d\n", rc);
        else
        {
            size_t buflen = sizeof(buf);
            coap_packet_t rsppkt;
#ifdef YACOAP_DEBUG
            coap_dump_packet(&pkt);
#endif
            coap_handle_request(resources, &pkt, &rsppkt);

            if ((rc = coap_build(&rsppkt, buf, &buflen)) > COAP_ERR)
                printf("coap_build failed rc=%d\n", rc);
            else
            {
#ifdef YACOAP_DEBUG
                printf("Sending: ");
                coap_dump(buf, buflen, true);
                printf("\n");
#endif
#ifdef YACOAP_DEBUG
                coap_dump_packet(&rsppkt);
#endif

                sendto(fd, buf, buflen, 0, (struct sockaddr *)&cliaddr, sizeof(cliaddr));
            }
        }
    }
}
