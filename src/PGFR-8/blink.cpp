/* The classic "blink" example
 *
 * This sample code is in the public domain.
 */
#include <stdlib.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "esp8266.h"

#include <string.h>

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"
#include "lwip/api.h"
#include "lwip/udp.h"
#include "lwip/igmp.h"

extern "C" {
#include "coap.h"
}

const int gpio = 2;

/* This task uses the high level GPIO API (esp_gpio.h) to blink an LED.
 *
 */
void blinkenTask(void *pvParameters)
{
    gpio_enable(gpio, GPIO_OUTPUT);
    while(1) {
        gpio_write(gpio, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_write(gpio, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}



void NET_UDP_rec(void *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, u16_t port)
{
    
}

bool UDP_Multicast_init(void)
{
    //bool IGMP_joined;
    struct ip_addr ipgroup;
    struct udp_pcb *g_udppcb;
    char msg[] = "gaurav";
    struct pbuf* p;
    p = pbuf_alloc(PBUF_TRANSPORT,sizeof(msg),PBUF_RAM);
    memcpy (p->payload, msg, sizeof(msg));

    struct ip_info local_ip;

    const uint8_t mode = sdk_wifi_get_opmode();

    if (mode & STATION_MODE) {
      sdk_wifi_get_ip_info(STATION_IF, &local_ip);
    } else {
      sdk_wifi_get_ip_info(SOFTAP_IF, &local_ip);
    }
    
    //224.0.1.186
    //IP4_ADDR(&ipgroup, 238, 0, 0, 3 ); //MultiCasting Ipaddress.
    IP4_ADDR(&ipgroup, 224, 0, 1, 186 ); //MultiCasting Ipaddress.
    g_udppcb =( struct udp_pcb*) udp_new();
    udp_bind(g_udppcb, IP_ADDR_ANY, 5683); //to receive multicast
    udp_recv(g_udppcb, NET_UDP_rec,NULL);// (void *)0); //NET_UDP_rec is the callback function that will be called every time you receive multicast

#if LWIP_IGMP
    int iret = igmp_joingroup(&local_ip.ip,(struct ip_addr *)(&ipgroup));
    printf("ret of igmp_joingroup: %d \n\r",iret);
#endif


    udp_sendto(g_udppcb,p,&ipgroup,5683); //send a multicast packet
    return iret == 0;
}

void coapTask(void *pvParameters);

void multicastTask(void *pvParameters)
{
    bool connected = false;
    
    for(;;)
    {
        vTaskDelay(10000 / portTICK_PERIOD_MS);
        if(!connected)
            connected = UDP_Multicast_init();
    }
}


extern "C" void user_init(void)
{
    uart_set_baud(0, 115200);
    
    //xTaskCreate(multicastTask, "multicastTask", 256, NULL, 2, NULL);
    xTaskCreate(blinkenTask, "blinkenTask", 256, NULL, 2, NULL);
    xTaskCreate(coapTask, "coapTask", 256, NULL, 2, NULL);
}
