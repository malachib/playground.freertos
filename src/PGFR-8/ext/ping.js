const coap = require('coap')
const _addr = process.env.ESP_IP;

if(_addr)
    addr = "coap://" + _addr + "/";
else
    addr = "coap://192.168.2.5/";

console.log("Pinging: " + addr);

var req = coap.request(addr + ".well-known/core");

req.on('response', res =>
{
    console.log('response code: ', res.code);
    res.pipe(process.stdout)
});

req.end();

var req = coap.request(addr + "/light");

req.on('response', res =>
{
    console.log('response code: ', res.code);
    res.pipe(process.stdout)
});

req.end();
