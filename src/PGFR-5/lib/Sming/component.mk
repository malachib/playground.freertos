INC_DIRS += $(sming_ROOT)Sming/Sming/SmingCore
sming_INC_DIR += $(sming_ROOT)Sming/Sming/SmingCore
sming_INC_DIR += $(sming_ROOT)Sming/Sming/include
sming_INC_DIR += $(sming_ROOT)Sming/Sming/system
sming_INC_DIR += $(sming_ROOT)Sming/Sming/system/include
sming_INC_DIR += $(sming_ROOT)Sming/Sming/system/include/espinc
sming_INC_DIR += $(ROOT)include/espressif/esp8266
# for osapi.h
sming_INC_DIR += $(ROOT)include/espressif
# for gpio.h
sming_INC_DIR += $(ROOT)core/include/esp
# for os_type.h
sming_INC_DIR += $(PROGRAM_DIR)/shim
# for espconn.h
sming_INC_DIR += $(ROOT)lwip/lwip/espressif/include/lwip/app

# OK don't try to pull in EVERYTHING just yet!!
#sming_SRC_DIR += $(sming_ROOT)Sming/Sming/SmingCore

$(eval $(call component_compile_rules,sming))

$(info INC_DIRS = ${INC_DIRS})
$(info Component root = ${sming_ROOT})
$(info Component inc_dir = ${sming_INC_DIR})
