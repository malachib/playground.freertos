// lifted and adapted from https://nodejs.org/api/dgram.html

const dgram = require('dgram');
const buf1 = Buffer.from('Some ');
const buf2 = Buffer.from('bytes');
const server = dgram.createSocket('udp4');

if(process.argv.length < 3) {
  _addr = '192.168.2.12';
} else {
  _addr = process.argv[2];
}

const addr = _addr;

server.on('error', (err) => {
  console.log(`server error:\n${err.stack}`);
  server.close();
});

server.on('message', (msg, rinfo) => {
  console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
});

server.on('listening', () => {
  var address = server.address();
  console.log(`server listening ${address.address}:${address.port}`);
  const client = dgram.createSocket('udp4');
  console.log(`sending packet to ${addr}`);
  client.send([buf1, buf2], 5000, addr, (err) => {
    client.close();
  });
});

server.bind(5001);
