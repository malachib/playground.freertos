extern "C" {

#include "espressif/esp_common.h"
#include "FreeRTOS.h"
#include "task.h"
#include <esp/uart.h>
}

#include <fact/iostream.h>
#include <string.h>
#include <stdio.h>

#include "lwip/api.hpp"
#include "lwip/udp.hpp"
#include "lwip/pbuf.hpp"

using namespace FactUtilEmbedded::std;

const int gpio = 2;


void blinkenTask(void *pvParameters)
{
    gpio_enable(gpio, GPIO_OUTPUT);
    while(1) {
        gpio_write(gpio, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_write(gpio, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void debugPrintBuffer(char* buffer, size_t len)
{
    clog << hex;
    while(len--) clog << " " << (uint16_t)(*buffer++);
    clog << dec;
}

void udpTask(void *pvParameters)
{
    struct ip_info local_ip;

    const uint8_t mode = sdk_wifi_get_opmode();

    if (mode & STATION_MODE) {
      sdk_wifi_get_ip_info(STATION_IF, &local_ip);
    } else {
      sdk_wifi_get_ip_info(SOFTAP_IF, &local_ip);
    }

    for(;;)
    {
#ifdef CPP_VERSION
        lwip::Netconn netconn(NETCONN_UDP);
        char buf[512];

        if(!netconn.isAllocated())
        {
            cerr << "Failed to allocate socket";
            abort();
        }

        netconn.bind(IP_ADDR_ANY, 5000);
        netconn.set_recvtimeout(0); // blocking

        lwip::Netbuf nbFrom(false);
        netconn.recv(nbFrom);

        void* _recvBuf;
        u16_t recvLen = 0;
        const size_t offset = 50;

        err_t err = nbFrom.data(&_recvBuf, &recvLen);

        //err_t err = nbFrom.copy(buf, sizeof(buf));
        //recvLen = nbFrom.len();
        //_recvBuf = buf;

        clog << "Status: " << (uint16_t)err << endl;
        clog << "Got " << recvLen << " bytes" << endl;
        debugPrintBuffer((char*)_recvBuf, recvLen);
        clog << endl;
        //clog << "Got: " << (char*)_recvBuf << endl;

        memset(buf, 0, sizeof(buf));
        char* offsetbuf = buf + offset;
        sprintf(offsetbuf, "Test send! (got: %d bytes)", recvLen);
        size_t buflen = strlen(offsetbuf);

        lwip::Netbuf nb(offsetbuf, buflen);

        clog << "About to send" << endl;

        clog << "buffer pre offset: ";
        debugPrintBuffer((char*)buf, offset);
        clog << endl;

        netconn.sendTo(nb, nbFrom.fromAddr(), 5001);

        clog << "buffer pre offset: ";
        debugPrintBuffer((char*)buf, offset);
        clog << endl;

        clog << "Sent" << endl;

        //nb.del();
        nbFrom.del();
        netconn.disconnect();
        netconn.del();

        clog << "All cleaned up";
#else
        struct netconn *conn = netconn_new_with_proto_and_callback(NETCONN_UDP, 0, NULL);
        char buf[512];

        if(!conn)
        {
            cerr << "Failed to allocate socket";
            abort();
        }

        netconn_bind(conn, IP_ADDR_ANY, 5000);
        netconn_set_recvtimeout(conn, 0); // blocking

        struct netbuf* nbFrom = netbuf_new();
        netconn_recv(conn, &nbFrom);

        void* _recvBuf;
        u16_t recvLen = 0;
        const size_t offset = 50;

        err_t err = netbuf_data(nbFrom, &_recvBuf, &recvLen);

        clog << "Status: " << (uint16_t)err << endl;
        clog << "Got " << recvLen << " bytes" << endl;
        debugPrintBuffer((char*)_recvBuf, recvLen);
        clog << endl;

        memset(buf, 0, sizeof(buf));
        char* offsetbuf = buf + offset;
        sprintf(offsetbuf, "Test send! (got: %d bytes)", recvLen);
        size_t buflen = strlen(offsetbuf);

        struct netbuf* nb = netbuf_new();
        netbuf_ref(nb, offsetbuf, buflen);

        clog << "About to send" << endl;

        clog << "buffer pre offset: ";
        debugPrintBuffer((char*)buf, offset);
        clog << endl;

        netconn_sendto(conn, nb, netbuf_fromaddr(nbFrom), 5001);

        clog << "buffer pre offset: ";
        debugPrintBuffer((char*)buf, offset);
        clog << endl;

        clog << "Sent" << endl;

        netbuf_delete(nb);
        netbuf_delete(nbFrom);
        netconn_disconnect(conn);
        netconn_delete(conn);
#endif
    }
}


extern "C" void user_init(void)
{
  uart_set_baud(0, 115200);

  xTaskCreate(blinkenTask, "blinkenTask", 256, NULL, 2, NULL);
  xTaskCreate(udpTask, "udpTask", 4096, NULL, 2, NULL);
}
