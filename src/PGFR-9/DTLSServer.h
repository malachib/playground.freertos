#pragma once

namespace wrapper
{

struct native_buffer
{
    uint8_t* data;
    size_t len;
};


struct managed_buffer
{
    size_t len;
    std::unique_ptr<uint8_t> data;
    
    managed_buffer(size_t len) : len(len), data(new uint8_t[len])
    {
        
    }
};

template<typename T>
struct buffer
{
    uint8_t* data() const;
    void data(uint8_t* d);
};


template<>
struct buffer<struct pbuf>
{
    struct pbuf* value;
public:
    buffer(struct pbuf* value) : value(value) {}
    
    uint8_t* data() const { return (uint8_t*) value->payload; }
};


template<typename T>
struct _buffer_data
{
    T value;
    typedef decltype(value.data) ptr_type;
    
    operator ptr_type& () { return value.data; }
    ptr_type data() const { return value.data; }
    void data(ptr_type d) { value.data = d; }
    size_t len() { return value.len; }
    void len(size_t len) { value.len = len; }
};

template<>
struct buffer<native_buffer&>
{
    native_buffer& value;
public:
    buffer(native_buffer& value) : value(value) {}
    
    uint8_t* data() const { return value.data; }
    void data(uint8_t* d) { value.data = d; }
    size_t len() { return value.len; }
    void len(size_t len) { value.len = len; }
};


template<>
struct buffer<managed_buffer>
{
    managed_buffer value;
public:
    buffer(size_t size) : value(size) {}
    
    uint8_t* data() const { return value.data.get(); }
    //void data(uint8_t* d) { value.data = d; }
    size_t len() { return value.len; }
    void len(size_t len) { value.len = len; }
};

/*
template<>
struct buffer<native_buffer>
{
    native_buffer value;
public:
    uint8_t* data() const { return value.data; }
    void data(uint8_t* d) { value.data = d; }
    size_t len() { return value.len; }
    void len(size_t len) { value.len = len; }
};*/

template<>
struct buffer<native_buffer> : public _buffer_data<native_buffer>
{
    
};

}

typedef wrapper::buffer<wrapper::native_buffer> native_buffer;
typedef wrapper::buffer<wrapper::managed_buffer> managed_buffer;

template <class T>
wrapper::buffer<T&> testwrap(T& in)
{
    return wrapper::buffer<T&>(in);
}


template <class T>
wrapper::buffer<T> testwrap2(T in)
{
    return wrapper::buffer<T>(in);
}


class DTLSServer
{
    /*
    TimingDelayContext timer;
    CookieContext cookie;
    EntropyContext entropy;
    SSLContext ssl;
    SSLConfig conf;
    X509Certificate srvcert;
    PrivateKeyContext pkey;
    NetContext listen_fd, client_fd;
    RandomGenerator ctr_drbg; */

public:
    DTLSServer()
    {
        
    }
    
    void iterate();
    
    enum UserLoopStatus
    {
        ReadErr,
        WriteErr,
        Continue,
        Exit
    };
    
    static int read(SSLContext& ssl, uint8_t* buffer, size_t len)
    {
        int ret;
        do 
        {
            ret = ssl.read(buffer, len);
            // DEBUG only code for nonblocking socket
            /*if(ret <= 0)
            {
                vTaskDelay(500 / portTICK_PERIOD_MS);
                clog << ".";
            } */
        }
        while(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE);
        return ret;
    }
    
    static int write(SSLContext& ssl, uint8_t* buffer, size_t len)
    {
        int ret;
        do ret = ssl.write(buffer, len);
        while(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE);
        return ret;
    }
    
    template <class T>
    static int read(SSLContext& ssl, wrapper::buffer<T> b)
    {
        return read(ssl, b.data(), b.len()-1);
    }
    
    template <class T>
    static int write(SSLContext& ssl, wrapper::buffer<T> b)
    {
        return write(ssl, b.data(), b.len());
    }
    
    virtual int bind(NetContext& listen_fd);
    virtual UserLoopStatus userLoop(SSLContext& ssl, int* ret);
};
