#include_next<lwipopts.h>

#define LWIP_IGMP 1
#define LWIP_RAND()                         rand()
//#define LWIP_DEBUG=1

// enabling this actually reduces output
//#define LWIP_DBG_TYPES_ON (LWIP_DBG_TRACE | LWIP_DBG_STATE)

#undef UDP_DEBUG
#define UDP_DEBUG                       LWIP_DBG_ON
