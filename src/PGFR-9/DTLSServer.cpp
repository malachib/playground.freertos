#include <stdlib.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
extern "C" {
#include "FreeRTOS.h"
#include "task.h"
#include "mbedtls/debug.h"
}
#include "esp8266.h"

#include <string.h>

// for itoa
#include <stdlib.h>

// This needs to go up here to avoid macro collisions with lwip-posix stuff
// will still encounter macro collision deeper in code if using lwip, so
// try to segregate mbedtls code into its own file away from lwip if possible
#include <MbedTLS.hpp>
#include <mbedtls/timing.hpp>
#include "mbedtls/net.hpp"
#include "lwip/api.hpp"
#include "lwip/udp.hpp"
#include <fact/iostream.h>
#include <SemaphoreCPP.h>

#include "lwip/err.h"
#include "lwip/igmp.h"

using namespace FactUtilEmbedded::std;
using namespace fact::mbedtls;

#include "overloads.h"

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#define mbedtls_printf     printf
#define mbedtls_fprintf    fprintf
#define mbedtls_time_t     time_t
#endif

extern char* server_key;
extern char* server_cert;

#include "DTLSServer.h"

#define ASSERT_EQUALS(expected, actual, action) \
{ \
    auto _e = expected; \
    auto _a = actual; \
    if(_e != _a) \
    {                           \
        cerr << "In Test#" << __COUNTER__ << " we expected " << _e << " but instead got " << _a; \
        { action; } \
    } \
}

#define CHECK_OK(actual) ASSERT_EQUALS(0, actual, goto exit)



// NOTE: I think I'm doing my custom timing wrong, if you looking at timing.c in the
// esp-open-rtos mbedtls library code, void* data is used to maintain distinct mbed_in_ms
//

// lifted from esp-open-rtos samples
static uint32_t get_current_time()
{
    return timer_get_count(FRC2) / 5000; // to get roughly 1 ms resolution
}

//#define DEBUG

#define TIMING_METHOD_2

// As initially described by https://tls.mbed.org/kb/how-to/dtls-tutorial
void mbedtls_timing_set_delay(void* data, uint32_t int_ms, uint32_t fin_ms)
{
    mbedtls_timing_delay_context* ctx = (mbedtls_timing_delay_context*) data;
    auto current_time = get_current_time();
#ifdef TIMING_METHOD_2
    ctx->int_ms = int_ms;
    ctx->fin_ms = fin_ms;

    memcpy(&ctx->timer, &current_time, sizeof(current_time));
#else
    ctx->int_ms = current_time + int_ms;
    ctx->fin_ms = fin_ms == 0 ? 0 : current_time + fin_ms;
#endif
#ifdef DEBUG
    clog << __func__ << " current seconds: " << (uint16_t)(current_time / 1000) << endl;
#endif
}


int mbedtls_timing_get_delay(void* data)
{
    mbedtls_timing_delay_context* ctx = (mbedtls_timing_delay_context*) data;
    if(ctx->fin_ms == 0) return -1;

    auto current_time = get_current_time();

#ifdef DEBUG
    clog << __func__ << " current seconds: " << (uint16_t)(current_time / 1000) << endl;
#endif

#ifdef TIMING_METHOD_2
    uint32_t last_time;

    memcpy(&last_time, &ctx->timer, sizeof(last_time));

    current_time -= last_time;
#endif
    if(current_time > ctx->fin_ms) return 2;
    else if(current_time > ctx->int_ms) return 1;
    else return 0;
}


int DTLSServer::bind(NetContext& listen_fd)
{
    return listen_fd.bind(NULL, "4433", MBEDTLS_NET_PROTO_UDP);
}

#ifdef MBEDTLS_DEBUG_C
char buf_err[256];

void my_debug( void *ctx, int level,
                      const char *file, int line,
                      const char *str )
{
    ((void) level);
    // bump up to render more of the filepath.  I keep it 0
    // for readability
    int pathdepth = 0;
    
    size_t filename_len = strlen(file);
    
    // retrieve only last subset of path (actual filename)
    for(int i = filename_len; i > 0; i--)
    {
        if(file[i] == '/')
        {
            if(pathdepth-- == 0)
            {
                file = &file[i + 1];
                break;
            }
        }
    }

    mbedtls_fprintf( (FILE *) ctx, "%s:%04d: %s", file, line, str );
    fflush(  (FILE *) ctx  );
}
#endif

#define USE_DEFAULTS

// Didn't use this yet, but a good Reference https://tls.mbed.org/kb/how-to/mbedtls-tutorial
// Clues for blocking/nonblocking timeout troubleshooting:
// https://github.com/SuperHouse/esp-open-rtos/issues/182
void DTLSServer::iterate()
{
    const char* pers = "dtls_server";

    //ip_addr_t client_ip;
    // pretty sure ipv4 we don't need a 16 byte buffer here, but we'll
    // be luxurious
    uint8_t client_ip[16] = { 0 };
    size_t cliip_len;

    TimingDelayContext timer;
    CookieContext cookie;
    EntropyContext entropy;
    SSLContext ssl;
    SSLConfig conf;
    mbedtls_ssl_config& _conf = conf;
    X509Certificate srvcert;
    PrivateKeyContext pkey;
    NetContext listen_fd, client_fd;
    RandomGenerator ctr_drbg;

    clog << "Loading the server cert & key for " << pers << endl;

    /*
    srvcert.parse(server_cert);
    //srvcert.parse(cas_pem); // TODO: I think this is a CA chain
    pkey.parseKey(server_key); */
    CHECK_OK(srvcert.parse(server_cert));
    CHECK_OK(pkey.parseKey(server_key))
    
    // TEST/DEBUG cert & PK chain.  Clearly we'll want better ones
    //CHECK_OK(srvcert.parse((uint8_t*)mbedtls_test_srv_crt, mbedtls_test_srv_crt_len));
    //CHECK_OK(srvcert.parse((uint8_t*)mbedtls_test_cas_pem, mbedtls_test_cas_pem_len));
    //CHECK_OK(pkey.parseKey((uint8_t*)mbedtls_test_srv_key, mbedtls_test_srv_key_len));

    mbedtls_pk_debug_item pk_debug_item;
    
    {
        char dbgBuf[128];
        
        mbedtls_pk_context& _pkey = pkey;
        mbedtls_x509_crt& _srvcert = srvcert;
        
        CHECK_OK(mbedtls_pk_debug(&_pkey, &pk_debug_item));
        mbedtls_x509_crt_info(dbgBuf, sizeof(dbgBuf), ":", &_srvcert);
    
        clog << "pk info: " << pk_debug_item.name << endl;
        clog << "crt info: " << dbgBuf << endl;
    }
    clog << "top of loop, heap memory free = " << xPortGetFreeHeapSize() << endl;
    
    CHECK_OK(bind(listen_fd));

    CHECK_OK(ctr_drbg.seed(entropy, pers));

    clog << "Setting up DTLS data...";

    CHECK_OK(conf.defaults(
        MBEDTLS_SSL_IS_SERVER,
        MBEDTLS_SSL_TRANSPORT_DATAGRAM,
        MBEDTLS_SSL_PRESET_DEFAULT));

    // Pretty sure we need to do this to enable CCM8 for californium
    mbedtls_ssl_conf_truncated_hmac(&_conf, MBEDTLS_SSL_TRUNC_HMAC_ENABLED);
#ifdef MBEDTLS_DEBUG_C
    mbedtls_ssl_conf_dbg( &_conf, my_debug, stdout );
#endif
    
    conf.readTimeout(30000);
    //conf.renegotiation(false);

    conf.setRng(ctr_drbg);
    
    //mbedtls_ssl_conf_dbg( &(mbedtls_ssl_config&)conf, my_debug, stdout );
    
    conf.caChain(*((mbedtls_x509_crt&)srvcert).next);

    CHECK_OK(conf.ownCert(srvcert, pkey)); // I think this is self signing it?

    CHECK_OK(cookie.setup(ctr_drbg));

    conf.dtlsCookies(&((mbedtls_ssl_cookie_ctx&)cookie));

    CHECK_OK(ssl.setup(conf));

    ssl.setTimerCb(&((mbedtls_timing_delay_context&)timer),
        mbedtls_timing_set_delay, mbedtls_timing_get_delay);

    clog << "ok" << endl;

reset:

    client_fd.free();

    ssl.reset();

    clog << "Waiting for a remote connection...";
    // wait for remote connection here - more socket/stream like than the
    // low level UDP protocol acts.  Seems prudent given DTLS semi-stream like
    // nature
    CHECK_OK(listen_fd.accept(client_fd, client_ip, sizeof(client_ip), &cliip_len));

    // Nonblock works too; For now, we're using blocking mode.  Easy
    // to use either, as long as you set up readTimeout & bio fully
    //client_fd.setNonBlock();
    
    // For HelloVerifyRequest cookies
    CHECK_OK(ssl.setClientTransportId(client_ip, cliip_len));

    ssl.setBIO(client_fd);

    clog << "ok" << endl;

    // 5. handshake
    clog << "Performing the DTLS handshake" << endl;

    int ret;

    clog << "heap memory free = " << xPortGetFreeHeapSize() << endl;

    do ret = ssl.handshake();
    while(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE);

    if(ret == MBEDTLS_ERR_SSL_HELLO_VERIFY_REQUIRED)
    {
        clog << "hello verification requested" << endl;
        ret = 0;
        goto reset;
    }
    else if(ret != 0)
    {
        clog << hex;
        clog << "failed-" << endl << "handshake returned " << -ret;
        clog << dec;
        //mbedtls_strerror(ret, buf_err, sizeof(buf_err));
        //clog << endl;
        //clog << "Error message: " << buf_err << endl;
        goto reset;
    }

    clog << "ok" << endl;

    // 6. read the echo request
    clog << "heap memory free = " << xPortGetFreeHeapSize() << endl;

    UserLoopStatus status;
    
    while((status = userLoop(ssl, &ret)) == UserLoopStatus::Continue);
    
    if(status == UserLoopStatus::ReadErr && ret <= 0)
    {
        switch(ret)
        {
            case MBEDTLS_ERR_SSL_TIMEOUT:
                clog << "timeout" << endl;
                goto reset;

            case MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY:
                clog << "connection closed gracefully" << endl;
                ret = 0;
                goto close_notify;

            default:
                clog << "mbedtls_ssl_read returned" << -ret << endl;
                goto reset;
        }
    }

    if(status == UserLoopStatus::WriteErr && ret < 0)
    {
        clog << "failed-" << endl;
        clog << "mbedtls_ssl_write returned " << -ret;
        goto exit;
    }

close_notify:
    clog << "Closing the connection...";

    do ret = ssl.closeNotify();
    while(ret == MBEDTLS_ERR_SSL_WANT_WRITE);
    ret = 0;

    clog << "done" << endl;
    goto reset;

    // Final clean-ups and exit
exit:
    client_fd.free();
    listen_fd.free();

    srvcert.free();
    pkey.free();
    ssl.free();
    conf.free();
    cookie.free();
    ctr_drbg.free();
    entropy.free();

    if(ret < 0) ret = 1;
}




DTLSServer::UserLoopStatus DTLSServer::userLoop(SSLContext& ssl, int* _ret)
{
    int& ret = *_ret;
    managed_buffer nb(512);
    auto _buf = (char*) nb.data();
    /*
    int len = 512;
    native_buffer nb;
    std::unique_ptr<char> _buf(new char[len]);
    auto buf = (uint8_t*)_buf.get();
    nb.len(len);
    nb.data((uint8_t*)_buf.get()); */
    /*
    wrapper::native_buffer __buf;
    __buf.len = 512;
    __buf.data = buf;
    wrapper::buffer<wrapper::native_buffer&> ___buf(__buf);
    auto ____buf = testwrap(__buf); */

    memset(nb.data(), 0, nb.len());
    //memset(buf, 0, len);

    clog << "< read from client: ";

    ret = read(ssl, nb);
    //ret = read(ssl, buf, len-1);

    if(ret < 0) return UserLoopStatus::ReadErr;

    //len = ret;
    nb.len(ret);
    clog << ret << " bytes read" << endl << endl;
    clog << _buf << endl << endl;

    // 7 write the 200 response (200? are we coap already ... ?)
    clog << "write to client: ";

    ret = write(ssl, nb);
    //ret = write(ssl, buf, len);
    
    if(ret < 0) return UserLoopStatus::WriteErr;

    clog << ret << " bytes written" << endl << endl;
    clog << _buf << endl << endl;

    return DTLSServer::Exit;
}
