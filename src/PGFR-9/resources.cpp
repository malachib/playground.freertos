#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "coap.hpp"

extern coap_resource_t resources[];

// TODO: move this to reused coap.cpp file
const coap_resource_path_t path_well_known_core = {2, {".well-known", "core"}};

#ifdef COAPCPP
yacoap::CoapManager<resources> coapManager;

int handle_get_well_known_core(const coap_resource_t *resource,
                                      const coap_packet_t *inpkt,
                                      coap_packet_t *pkt)
{
    return coapManager.handle_get_well_known_core(resource, inpkt, pkt);
}

/*
//template <template<const coap_resource_t* resources, size_t resp_len = 128> yacoap::CoapManager& manager>
//template <auto coapManager>
template <yacoap::CoapManager<template<
    const coap_resource_t* resources,
    size_t resp_len>>
    coapManager>
int handle_get_well_known_core_test(const coap_resource_t *resource,
                                      const coap_packet_t *inpkt,
                                      coap_packet_t *pkt)
{
    return coapManager.handle_get_well_known_core(resource, inpkt, pkt);
}
*/

#else

const uint16_t rsplen = 128;
static char rsp[128] = "";

void resource_setup(const coap_resource_t *resources)
{
    coap_make_link_format(resources, rsp, rsplen);
    printf("resources: %s\n", rsp);
}

static int handle_get_well_known_core(const coap_resource_t *resource,
                                      const coap_packet_t *inpkt,
                                      coap_packet_t *pkt)
{
    printf("handle_get_well_known_core\n");
    return coap_make_response(inpkt->hdr.id, &inpkt->tok,
                              COAP_TYPE_ACK, COAP_RSPCODE_CONTENT,
                              resource->content_type,
                              (const uint8_t *)rsp, strlen(rsp),
                              pkt);
}
#endif

static const coap_resource_path_t path_light = {1, {"light"}};
static char light = '0';

static int handle_get_light(const coap_resource_t *resource,
                            const coap_packet_t *inpkt,
                            coap_packet_t *pkt)
{
    printf("handle_get_light\n");
#ifdef COAPCPP
    return yacoap::coap_make_content_response(inpkt, resource,
                              (const uint8_t *)&light, 1,
                              pkt);
#else
    return coap_make_response(inpkt->hdr.id, &inpkt->tok,
                              COAP_TYPE_ACK, COAP_RSPCODE_CONTENT,
                              resource->content_type,
                              (const uint8_t *)&light, 1,
                              pkt);
#endif
}

static int handle_put_light(const coap_resource_t *resource,
                            const coap_packet_t *inpkt,
                            coap_packet_t *pkt)
{
    printf("handle_put_light\n");
    if (inpkt->payload.len == 0) {
        return coap_make_response(inpkt->hdr.id, &inpkt->tok,
                                  COAP_TYPE_ACK, COAP_RSPCODE_BAD_REQUEST,
                                  NULL, NULL, 0,
                                  pkt);
    }
    if (inpkt->payload.p[0] == '1') {
        light = '1';
        printf("Light ON\n");
    }
    else {
        light = '0';
        printf("Light OFF\n");
    }
    return coap_make_response(inpkt->hdr.id, &inpkt->tok,
                              COAP_TYPE_ACK, COAP_RSPCODE_CHANGED,
                              resource->content_type,
                              (const uint8_t *)&light, 1,
                              pkt);
}


coap_resource_t resources[] =
{
    // Can't quite figure out how to do this... feels close
#ifdef _COAPCPP
    {COAP_RDY, COAP_METHOD_GET, COAP_TYPE_ACK,
        yacoap::CoapManager<resources>::_handle_get_well_known_core, &path_well_known_core,
        COAP_SET_CONTENTTYPE(COAP_CONTENTTYPE_APP_LINKFORMAT)},
#else
    COAP_RESOURCE_WELLKNOWN(handle_get_well_known_core),
#endif
    COAP_RESOURCE_GET(handle_get_light, &path_light, COAP_CONTENTTYPE_TXT_PLAIN),
    {COAP_RDY, COAP_METHOD_PUT, COAP_TYPE_ACK,
        handle_put_light, &path_light,
        COAP_SET_CONTENTTYPE(COAP_CONTENTTYPE_NONE)},
    COAP_RESOURCE_NULL
};
