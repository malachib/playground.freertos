#include <stdlib.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
extern "C" {
#include "FreeRTOS.h"
#include "task.h"
}
#include "esp8266.h"

#include <string.h>

// for itoa
#include <stdlib.h>

// This needs to go up here to avoid macro collisions with lwip-posix stuff
// will still encounter macro collision deeper in code if using lwip, so
// try to segregate mbedtls code into its own file away from lwip if possible
#include <MbedTLS.hpp>
#include <mbedtls/timing.hpp>
#include <mbedtls/net.hpp>
#include "lwip/api.hpp"
#include "lwip/udp.hpp"
#include <fact/iostream.h>
#include <SemaphoreCPP.h>
#include <coap.hpp>

#include "lwip/err.h"
//#include "lwip/sockets.h"
//#include "lwip/sys.h"
//#include "lwip/netdb.h"
//#include "lwip/dns.h"
#include "lwip/igmp.h"

using namespace FactUtilEmbedded::std;
using namespace fact::mbedtls;
using namespace yacoap;

#include "overloads.h"

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#define mbedtls_printf     printf
#define mbedtls_fprintf    fprintf
#define mbedtls_time_t     time_t
#endif

#include "DTLSServer.h"

extern char* server_key;
extern char* server_cert;

template<int* pci> struct X{};
template<int* pci> void tester();

//constexpr static int  _slide[1];


#define ASSERT_EQUALS(expected, actual, action) \
{ \
    auto _e = expected; \
    auto _a = actual; \
    if(_e != _a) \
    {                           \
        clog << "In Test#" << __COUNTER__ << " we expected " << _e << " but instead got " << _a; \
        { action; } \
    } \
}

#define CHECK_OK(actual) ASSERT_EQUALS(0, actual, goto exit)
/*    \
{ \
    goto exit; \
})*/

extern Semaphore wifi_alive;



#ifdef MBEDTLS_DEBUG_C
void my_debug( void *ctx, int level,
                      const char *file, int line,
                      const char *str );
#endif



// Reference: https://github.com/ARMmbed/mbedtls/blob/master/programs/ssl/dtls_server.c
void coapsTask(void *pvParameters)
{
#ifdef MBEDTLS_DEBUG_C
    mbedtls_debug_set_threshold(1); // 4 = most verbose
#endif
   // sticking this in the loop causes failures :(
    wifi_alive.take();

    //static char buf[1024];

    for(;;)
    {
        const char* pers = "dtls_server";

        //ip_addr_t client_ip;
        // pretty sure ipv4 we don't need a 16 byte buffer here, but we'll
        // be luxurious
        uint8_t client_ip[16] = { 0 };
        size_t cliip_len;
        char* buf = NULL;
        int len = 512;
        //int len = sizeof(buf) - 1;

        //static int _slide[10];
        //X<_slide> xi;
        //constexpr int* slider;
        //tester<_slide>();

        TimingDelayContext timer;
        CookieContext cookie;
        EntropyContext entropy;
        SSLContext ssl;
        SSLConfig conf;
        X509Certificate srvcert;
        PrivateKeyContext pkey;
        NetContext listen_fd, client_fd;
        RandomGenerator ctr_drbg;

        clog << "Loading the server cert & key" << endl;

        /*
        srvcert.parse(server_cert);
        //srvcert.parse(cas_pem); // TODO: I think this is a CA chain
        pkey.parseKey(server_key); */
        // TEST/DEBUG cert & PK chain.  Clearly we'll want better ones
        CHECK_OK(srvcert.parse((uint8_t*)mbedtls_test_srv_crt, mbedtls_test_srv_crt_len));
        CHECK_OK(srvcert.parse((uint8_t*)mbedtls_test_cas_pem, mbedtls_test_cas_pem_len));
        CHECK_OK(pkey.parseKey((uint8_t*)mbedtls_test_srv_key, mbedtls_test_srv_key_len));

        clog << "top of loop, heap memory free = " << xPortGetFreeHeapSize() << endl;

        CHECK_OK(listen_fd.bind(NULL, "4433", MBEDTLS_NET_PROTO_UDP)); // TODO: change to CoAP port

        CHECK_OK(ctr_drbg.seed(entropy, pers));

        clog << "Setting up DTLS data...";

        CHECK_OK(conf.defaults(
            MBEDTLS_SSL_IS_SERVER,
            MBEDTLS_SSL_TRANSPORT_DATAGRAM,
            MBEDTLS_SSL_PRESET_DEFAULT));

        conf.setRng(ctr_drbg);

#ifdef MBEDTLS_DEBUG_C
        mbedtls_ssl_conf_dbg( &(mbedtls_ssl_config&)conf, my_debug, stdout );
#endif

        conf.caChain(*((mbedtls_x509_crt&)srvcert).next);

        CHECK_OK(conf.ownCert(srvcert, pkey)); // I think this is self signing it?

        CHECK_OK(cookie.setup(ctr_drbg));

        conf.dtlsCookies(&((mbedtls_ssl_cookie_ctx&)cookie));

        CHECK_OK(ssl.setup(conf));

        ssl.setTimerCb(&((mbedtls_timing_delay_context&)timer),
            mbedtls_timing_set_delay, mbedtls_timing_get_delay);

        clog << "ok" << endl;

reset:
        clog << "@reset: heap memory free = " << xPortGetFreeHeapSize() << endl;

        client_fd.free();

        ssl.reset();

        clog << "Waiting for a remote connection...";
        // wait for remote connection here - more socket/stream like than the
        // low level UDP protocol acts.  Seems prudent given DTLS semi-stream like
        // nature
        CHECK_OK(listen_fd.accept(client_fd, client_ip, sizeof(client_ip), &cliip_len));

        // For HelloVerifyRequest cookies
        CHECK_OK(ssl.setClientTransportId(client_ip, cliip_len));

        ssl.setBIO(client_fd);

        clog << "ok" << endl;

        // 5. handshake
        clog << "Performing the DTLS handshake" << endl;

        int ret;

        clog << "heap memory free = " << xPortGetFreeHeapSize() << endl;

        do ret = ssl.handshake();
        while(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE);

        if(ret == MBEDTLS_ERR_SSL_HELLO_VERIFY_REQUIRED)
        {
            clog << "hello verification requested" << endl;
            ret = 0;
            goto reset;
        }
        else if(ret != 0)
        {
            clog << "failed-" << endl << "handshake returned " << -ret;
#ifdef DEBUG
            mbedtls_strerror(ret, buf_err, sizeof(buf_err));
            clog << endl;
            clog << "Error message: " << buf_err << endl;
#endif
            goto reset;
        }

        clog << "ok" << endl;

        // 6. read the echo request
        clog << "heap memory free = " << xPortGetFreeHeapSize() << endl;
        clog << "< read from client: ";

        buf = new char[len];
        memset(buf, 0, len);

        do ret = ssl.read((uint8_t*)buf, len-1);
        while(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE);

        if(ret <= 0)
        {
            delete[] buf;
            buf = NULL;
            switch(ret)
            {
                case MBEDTLS_ERR_SSL_TIMEOUT:
                    clog << "timeout" << endl;
                    goto reset;

                case MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY:
                    clog << "connection closed gracefully" << endl;
                    ret = 0;
                    goto close_notify;

                default:
                    clog << "mbedtls_ssl_read returned" << -ret << endl;
                    goto reset;
            }
        }

        len = ret;
        clog << len << " bytes read" << endl << endl;
        clog << buf << endl << endl;

        // 7 write the 200 response (200? are we coap already ... ?)
        clog << "write to client: ";

        do ret = ssl.write((uint8_t*)buf, len);
        while(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE);

        if(ret < 0)
        {
            clog << "failed-" << endl;
            clog << "mbedtls_ssl_write returned " << -ret;
            goto exit;
        }

        len = ret;
        clog << len << " bytes written" << endl << endl;
        clog << buf << endl << endl;

close_notify:
        delete buf;

        clog << "Closing the connection...";

        do ret = ssl.closeNotify();
        while(ret == MBEDTLS_ERR_SSL_WANT_WRITE);
        ret = 0;

        clog << "done" << endl;
        goto reset;

        // Final clean-ups and exit
exit:
        if(buf != NULL)
        {
            delete buf;
        }

        client_fd.free();
        listen_fd.free();

        srvcert.free();
        pkey.free();
        ssl.free();
        conf.free();
        cookie.free();
        ctr_drbg.free();
        entropy.free();

        if(ret < 0) ret = 1;

        // don't return since FreeRTOS doesn't like that
        //return ret;
    }
}


template  <class TConnection, class TSocket>
class NetFactory
{

};




template <class TNetFactory>
class COAPBlockingServer
{

};


template <const coap_resource_t* resources, size_t rsplen = 128>
class DTLSCoAPServer : public DTLSServer
{
    CoapManager<resources, rsplen> manager;

public:
    UserLoopStatus userLoop(SSLContext& ssl, int* _ret) override;

    int handle_get_well_known_core(const coap_resource_t *resource,
                                          const coap_packet_t *inpkt,
                                          coap_packet_t *pkt)
    {
        return manager.handle_get_well_known_core(resource, inpkt, pkt);
    }

    int bind(NetContext& listen_fd) override
    {
        return listen_fd.bind(NULL, "5684", MBEDTLS_NET_PROTO_UDP);
    }
};



template <const coap_resource_t* resources, size_t rsplen>
DTLSServer::UserLoopStatus DTLSCoAPServer<resources, rsplen>::userLoop(SSLContext& ssl, int* _ret)
{
    const size_t _buflen = 512;
    size_t buflen = _buflen;
    std::unique_ptr<char> _buf(new char[_buflen]);
    auto buf = (uint8_t*) _buf.get();
    int& ret = *_ret;

    ret = read(ssl, buf, buflen);

    if(ret < 0) return UserLoopStatus::ReadErr;

    buflen = ret;

#ifdef YACOAP_DEBUG
    printf("Received: ");
    coap_dump(buf, buflen, true);
    printf("\n");
#endif

    // this parses the buffer
    CoapRequest request(buf, buflen);

    if (request.getResult() > COAP_ERR)
        clog << "Bad packet rc=" << request.getResult() << endl;
    else
    {
        int rc;
        //size_t buflen = sizeof(buf);
        CoapPacket response;
#ifdef YACOAP_DEBUG
        request.dump();
#endif

        manager.handleRequest(request, response);

        if ((rc = response.build(buf, &buflen)) > COAP_ERR)
            printf("coap_build failed rc=%d\n", rc);
        else
        {
#ifdef YACOAP_DEBUG
            clog << "Sending: " << (uint16_t) buflen << endl;
            coap_dump(buf, buflen, true);
            clog << endl;
#endif
#ifdef YACOAP_DEBUG
            response.dump();
#endif

            ret = write(ssl, buf, buflen);

            if(ret < 0) return UserLoopStatus::WriteErr;
        }
    }

    return UserLoopStatus::Continue;
    //return UserLoopStatus::Exit;
};

extern coap_resource_t coaps_resources[];

DTLSCoAPServer<coaps_resources> coapsServer;

int coaps_handle_get_well_known_core(const coap_resource_t *resource,
                                      const coap_packet_t *inpkt,
                                      coap_packet_t *pkt)
{
    return coapsServer.handle_get_well_known_core(resource, inpkt, pkt);
}

static const coap_resource_path_t path_secure = {1, {"secure"}};
char secureFlag = '0';

static int handle_put_secure(const coap_resource_t *resource,
                            const coap_packet_t *inpkt,
                            coap_packet_t *pkt)
{
    clog << __func__ << endl;

    if (inpkt->payload.len == 0)
        return coap_make_badrequest_response(inpkt, pkt);

    clog << "Secure ";

    if (inpkt->payload.p[0] == '1') {
        secureFlag = '1';
        clog << "ON";
    }
    else {
        secureFlag = '0';
        clog << "OFF";
    }
    clog << endl;
    return coap_make_response(inpkt->hdr.id, &inpkt->tok,
                              COAP_TYPE_ACK, COAP_RSPCODE_CHANGED,
                              resource->content_type,
                              (const uint8_t *)&secureFlag, 1,
                              pkt);
}



coap_resource_t coaps_resources[] =
{
    COAP_RESOURCE_WELLKNOWN(coaps_handle_get_well_known_core),
    COAP_RESOURCE_PUT(handle_put_secure, &path_secure, COAP_CONTENTTYPE_TXT_PLAIN),
    COAP_RESOURCE_NULL
};



void coapsTask2(void *pvParameters)
{
#ifdef MBEDTLS_DEBUG_C
    mbedtls_debug_set_threshold(3); // 4 = most verbose
#endif

    // sticking this in the loop causes failures :(
    wifi_alive.take();

    for(;;)
    {
        /*
        DTLSServer dtls;

        dtls.iterate();*/
        coapsServer.iterate();
    }
}
