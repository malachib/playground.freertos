#pragma once

namespace std
{
    // shim for lack of C++ lib
    template <class T>
    class unique_ptr
    {
        T* value;
    public:
        unique_ptr(T* value) : value(value) {}
        ~unique_ptr() { delete value; }
        
        T& operator *() const { return *value; }
        
        T* get() const { return value; }
    };
}

inline basic_ostream<char>& operator<<(basic_ostream<char>& out, ip_addr_t& addr)
{
    out << (uint16_t) ip4_addr1(&addr);
    out << '.';
    out << (uint16_t) ip4_addr2(&addr);
    out << '.';
    out << (uint16_t) ip4_addr3(&addr);
    out << '.';
    out << (uint16_t) ip4_addr4(&addr);
    return out;
}

inline basic_ostream<char>& operator<<(basic_ostream<char>& out, unsigned int value)
{
    return out << (uint16_t) value;
}


template <class T>
inline basic_ostream<char>& operator<<(basic_ostream<char>& out, std::unique_ptr<T> ptr)
{
    return out << ptr.get();
}
