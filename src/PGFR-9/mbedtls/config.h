#define MBEDTLS_DEBUG_C

#include_next<mbedtls/config.h>

// Pretty sure we don't need these
//#undef MBEDTLS_SSL_MAX_FRAGMENT_LENGTH
#undef MBEDTLS_SSL_PROTO_SSL3
//#undef MBEDTLS_SSL_PROTO_TLS1

#undef MBEDTLS_DES_C
#undef MBEDTLS_BLOWFISH_C

// Might need these
//#undef MBEDTLS_ASN1_PARSE_C
//#undef MBEDTLS_ASN1_WRITE_C

// Fully experimental
#undef MBEDTLS_SSL_MAX_CONTENT_LEN
#define MBEDTLS_SSL_MAX_CONTENT_LEN             2500
