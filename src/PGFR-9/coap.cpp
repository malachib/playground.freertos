#include <stdlib.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
extern "C" {
#include "FreeRTOS.h"
#include "task.h"
}
#include "esp8266.h"

#include <string.h>

// for itoa
#include <stdlib.h>

// This needs to go up here to avoid macro collisions with lwip-posix stuff
// will still encounter macro collision deeper in code if using lwip, so
// try to segregate mbedtls code into its own file away from lwip if possible
#include <MbedTLS.hpp>
#include <mbedtls/timing.hpp>
#include "lwip/api.hpp"
#include "lwip/udp.hpp"
#include <fact/iostream.h>
#include <SemaphoreCPP.h>

#include "lwip/err.h"
//#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
//#include "lwip/dns.h"
#include "lwip/igmp.h"

using namespace FactUtilEmbedded::std;

#if YACOAP_DEBUG
#include "coap_dump.h"
#endif

#ifdef POSIX
#error "POSIX shouldn't be enabled here"
#endif

#undef bind
#undef listen
#undef accept
#undef write
#undef recv
#undef read

#include "coap.hpp"

extern coap_resource_t resources[];

#ifdef COAPCPP
extern yacoap::CoapManager<resources> coapManager;
#else
void resource_setup(const coap_resource_t *resources);
#endif


#include "mbedtls/ssl_cookie.hpp"



#include "overloads.h"

extern Semaphore wifi_alive;
static uint8_t buf[1024]; // beware, not thread safe

void coapTask(void *pvParameters)
{
    // sticking this in the loop causes failures :(
    wifi_alive.take();

    cout << "coap_packet_t size = " << (uint16_t)sizeof(coap_packet_t) << endl;

    while(1)
    {
        cout << "Waiting..." << endl;

        //fact::mbedtls::CookieContext cookie;


        cout << "Waiting...0" << endl;

        lwip::Netconn netconn(NETCONN_UDP);

        if(!netconn.isAllocated())
        {
            cerr << "Failed to allocate socket";
            abort();
        }

        /*
        ip_addr_t groupIP;
        ip_info local_ip;

        sdk_wifi_get_ip_info(STATION_IF, &local_ip);
        IP4_ADDR(&groupIP, 224, 0, 1, 186);
        err_t igmp_err = netconn_join_leave_group(netconn, &groupIP, &local_ip.ip, NETCONN_JOIN);

        // This returns -6 just like the other one.  At least we're consistent
        cout << "Attempt to join IGRP: " << (int16_t) igmp_err << endl;
        */

        // looks like we might not have to tear down entire netconn each time,
        // but until I get more experience I am gonna play it safe
        netconn.bind(IP_ADDR_ANY, COAP_DEFAULT_PORT);
        netconn.set_recvtimeout(0); // blocking
        //cout << "Waiting...1" << endl;

#ifndef COAPCPP
        resource_setup(resources);
#endif
        int n, rc;
        // If I put this pkt and the other one on the stack, high speed requests
        // crash the device
#ifdef COAPCPP
        static yacoap::CoapPacket _pkt;
#else
        static coap_packet_t pkt;
#endif

        cout << "Waiting...2" << endl;
        lwip::Netbuf nbClient(false);
        err_t err = netconn.recv(nbClient);
        //cout << "Waiting...2.3: err code = " << (uint16_t)err << endl;
        //TODO: operate directly on buffer
        u16_t copied_len = nbClient.copy(buf, sizeof(buf));
        void* chainBuf;
        u16_t chainLen;
        nbClient.data(&chainBuf, &chainLen);
        n = nbClient.len();

        cout << "Chain len: " << (uint16_t) chainLen << endl;
        cout << "Copied len: " << (uint16_t) copied_len << endl;
        cout << "calc len: " << (uint16_t) n << endl;

#ifdef YACOAP_DEBUG
        printf("Received: ");
        coap_dump(buf, n, true);
        printf("\n");
#endif

#ifdef COAPCPP
        if ((rc = _pkt.parse(buf, n)) > COAP_ERR)
#else
        if ((rc = coap_parse(buf, n, &pkt)) > COAP_ERR)
#endif
            printf("Bad packet rc=%d\n", rc);
        else
        {
            size_t buflen = sizeof(buf);
#ifdef COAPCPP
            static yacoap::CoapPacket _rsppkt;
#ifdef YACOAP_DEBUG
            _pkt.dump();
#endif
            //coapManager.handleRequest(&pkt, &rsppkt);
            coapManager.handleRequest(_pkt, _rsppkt);
#else
            static coap_packet_t rsppkt;
#ifdef YACOAP_DEBUG
            coap_dump_packet(&pkt);
#endif
            coap_handle_request(resources, &pkt, &rsppkt);
#endif

#ifdef COAPCPP
            if ((rc = _rsppkt.build(buf, &buflen)) > COAP_ERR)
#else
            if ((rc = coap_build(&rsppkt, buf, &buflen)) > COAP_ERR)
#endif
                printf("coap_build failed rc=%d\n", rc);
            else
            {
#ifdef YACOAP_DEBUG
                printf("Sending: ");
                coap_dump(buf, buflen, true);
                printf("\n");
#endif
#ifdef YACOAP_DEBUG
#ifdef COAPCPP
                _rsppkt.dump();
#else
                coap_dump_packet(&rsppkt);
#endif
#endif

                lwip::Netbuf nb(buf, buflen);
                //netbuf* _nb2 = netbuf_new();
                auto addr = nbClient.fromAddr();
                uint16_t port = nbClient.fromPort();
                cout << "addr: " << *addr << endl;
                cout << "port: " << port << endl;
                //cout << "_nb2: " << (void*)_nb2 << endl;
                cout << "buf: " << (void*)buf << "length: " << (uint16_t)buflen << endl;
                //err_t err = netbuf_ref(_nb2, buf, buflen);
                cout << "netbuf_ref result: " << (uint16_t)err << endl;
                netconn.sendTo(nb, nbClient);
                //netconn.sendTo(nb, addr, port);
                //netconn.sendTo(_nb2, addr, port);
                //err = netconn_sendto(netconn, _nb2, addr, port);
                cout << "netconn_sendto result: " << (uint16_t)err << endl;

                cout << "Sending it out" << endl;

                // This delete operation crashes it... not sure why
                //netbuf_delete(_nb2);
                // not well documented, but seems that ref bufs you call FREE instead
                // of delete...
                //cout << "_nb2: " << (void*)_nb2 << endl;
                //netbuf_free(_nb2);
            }
        }

        cout << "Finishing up 1" << endl;

        nbClient.del();

        cout << "Finishing up 2" << endl;

        netconn.disconnect();
        netconn.del();
    }
}
