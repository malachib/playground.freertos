#include <netsocket/WiFiAccessPoint.h>

// Adapting from https://docs.mbed.com/docs/getting-started-mbed-os/en/latest/Full_Guide/networking/
//#include "sockets/v0/UDPSocket.h"
#include <netsocket/UDPSocket.h>
//#include "sal-stack-lwip/lwipv4_init.h"
//#include "sal-iface-eth/EthernetInterface.h"

extern "C" {
    
#include "FreeRTOS.h"
#include "task.h"
    
}

// Lifted out of LWIP support area in mbed
#define IP4ADDR_STRLEN_MAX  16
#define IPADDR_STRLEN_MAX   IP4ADDR_STRLEN_MAX

//#include "ip4_addr.h"
//#include "ipv4/lwip/ip_addr.h"
#include "EthernetInterface.h"
WiFiAccessPoint test_ap1;

/*
class Resolver {
private:
    UDPSocket _sock;
public:
    Resolver() : _sock(SOCKET_STACK_LWIP_IPV4) {
        _sock.open(SOCKET_AF_INET4);
    }
    void onDNS(Socket *s, struct socket_addr addr, const char *domain) {
        (void) s;
        SocketAddr sa;
        char buf[16];
        sa.setAddr(&addr);
        sa.fmtIPv4(buf,sizeof(buf));
        printf("Resolved %s to %s\r\n", domain, buf);
    }
    
    socket_error_t resolve(const char * address) {
        printf("Resolving %s...\r\n", address);
        // MB: Looks like another potentially shitty example, UDPSocket::resolve
        // API doesn't even seem to exist in header files for mbed OS now. thanks guys.
        return _sock.resolve(address,
            UDPSocket::DNSHandler_t(this, &Resolver::onDNS));
    }
};
*/
void test_ap1_test()
{
    //test_ap1.
}

EthernetInterface eth;

void mbed_udp_task(void *pvParameters)
{
    for(;;)
    {
        vTaskDelay(5000 / portTICK_PERIOD_MS);
        //2. Try to connect
        eth.connect();            // <- This line will not work now,
                                  //    but at least it will help you to find out your
                                  //    own MAC-address.

        //3. Print the MAC-address
        //logger.
            printf("Controller MAC Address is: %s\r\n", eth.get_mac_address());
    }
}
