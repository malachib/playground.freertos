extern "C" {

#include "espressif/esp_common.h"
#include "FreeRTOS.h"
#include "task.h"
#include <esp/uart.h>

}

const int gpio = 2;


void blinkenTask(void *pvParameters)
{
    gpio_enable(gpio, GPIO_OUTPUT);
    while(1) {
        gpio_write(gpio, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_write(gpio, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}


// Can't enable this yet due to linker issues (mbed shim lib not pulling in all
// c/cpp code).  that is probably a big one
//void mbed_udp_task(void *pvParameters);

extern "C" void user_init(void)
{
  uart_set_baud(0, 115200);


  // It looks like prio 1 is an idle task, not just 0 - setting
  // blinkenTask to prio 1 = it gets fully blocked out by web server task
  xTaskCreate(blinkenTask, "blinkenTask", 256, NULL, 2, NULL); 
  xTaskCreate(mbed_udp_task, "mbed OS task", 1024, NULL, 2, NULL);
}
