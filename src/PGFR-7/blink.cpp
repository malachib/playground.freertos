/* The classic "blink" example
 *
 * This sample code is in the public domain.
 */
#include <stdlib.h>
//#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "esp8266.h"

//#include <fact/debug.h>
#include <fact/iostream.h>
#include <Console.h>
#include <fact/IMenu.h>
#include <fact/Menu.h>
#include <fact/MenuFunction.h>

namespace util = FactUtilEmbedded;
using namespace util::std;
using namespace util;

const int gpio = 2;

/* This task uses the high level GPIO API (esp_gpio.h) to blink an LED.
 *
 */
void blinkenTask(void *pvParameters)
{
    gpio_enable(gpio, GPIO_OUTPUT);
    while(1) {
        gpio_write(gpio, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_write(gpio, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}


void test(IMenu::Parameters p)
{
  cout << "Hi from test!" << endl;
}

void test2(const char* input)
{
  cout << "I got: " << input << endl;
}

void anotherTest(const char* input)
{
  cout << "I really got: " << input << endl;
}

MenuGeneric menuGeneric(test);
CREATE_MENUFUNCTION(menuTest2, test2, "test entry #2");
CREATE_MENUFUNCTION(menuAnotherTest, anotherTest, "test entry #3");

class MainMenu : public Menu
{
public:
  MainMenu()
  {
    add(menuGeneric, F("test"), F("test entry"));
    add(menuTest2);
    add(menuAnotherTest);
  }
};

MainMenu mainMenu;
ConsoleMenu console(&mainMenu);


void consoleTask(void *pvParameters)
{
    for(;;)
    {
      /*
      if(Serial.available())
      {
        Serial << "character available: ";
        Serial << (char)Serial.read();
      }*/
      console.handler();
    }
}


extern "C" void user_init(void)
{
    uart_set_baud(0, 115200);
    xTaskCreate(blinkenTask, "blinkenTask", 256, NULL, 2, NULL);
    xTaskCreate(consoleTask, "consoleTask", 256, NULL, 2, NULL);
}
