#define LWIP_IGMP 1

// This approach seems to be esp-open-rtos specific
// meaning that the actual lwip2 stuff isn't necessarily affected by this

#define LWIP_HTTPD_CGI 1
#define LWIP_HTTPD_SSI 1
#define HTTPD_ENABLE_HTTPS 1

// Until we get newer mbedtls with MBEDTLS_ERR_SSL_CLIENT_RECONNECT
// we can't enable this
#if HTTPD_ENABLE_HTTPS
#define LWIP_ALTCP 1
#define LWIP_ALTCP_TLS 1
#define LWIP_ALTCP_TLS_MBEDTLS 1
#endif

#define LWIP_MDNS_RESPONDER 1

// mDNS needs this
// Enabling this does similar things as enabling LWIP_IGMP , basically breaks offset
// for hwaddr
// appears to shift everything down 32 bits
#define LWIP_NUM_NETIF_CLIENT_DATA 2

// NOTE: Not sure if this one is needed
//#define LWIP_RAND()                         rand()

#include_next <lwipopts.h>

#undef TCPIP_THREAD_STACKSIZE
#define TCPIP_THREAD_STACKSIZE  768
//#define LWIP_DEBUG 1
//#define LWIP_SDK_DEBUG 1
//#define MDNS_DEBUG LWIP_DBG_TRACE

//#undef IP_DEBUG
//#define IP_DEBUG LWIP_DBG_TRACE
