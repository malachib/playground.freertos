#!/bin/bash

git submodule init
git submodule update

RTOS_VERSION=V9.0.0
mkdir ext
pushd ext
# Not presently using these because presently the FreeRTOS+TCP zip contains FreeRTOS itself
# but it's unclear what version/if they've modified it, so using theirs
# until they've sorted out exposing the FreeRTOS+TCP repo
svn export svn://svn.code.sf.net/p/freertos/code/tags/$RTOS_VERSION/FreeRTOS/Source FreeRTOS
svn export svn://svn.code.sf.net/p/freertos/code/tags/$RTOS_VERSION/FreeRTOS-Plus/Source FreeRTOS-Plus

pushd esp-open-sdk
git submodule init
git submodule update
popd
pushd esp-open-rtos
git submodule init
git submodule update
popd

# Remember for ESP8266 builds, you need to visit and follow the instructions at:
# https://github.com/pfalcon/esp-open-sdk/
# https://github.com/SuperHouse/esp-open-rtos

# TODO: check MACHTYPE against x86_64-apple-darwin16 (x86_64-apple-darwinXXX better)
# and if so, then check for case sensitive file system - and if THAT is true
# do the hdiutil trick
