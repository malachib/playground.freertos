#!/bin/bash

export PATH=$PATH:$PWD/ext/esp-open-sdk/xtensa-lx106-elf/bin
export ESP_OPEN_RTOS=$PWD/ext/esp-open-rtos
export PLAYGROUND_FREERTOS=$PWD

US=./ext/useful-scripts/embedded

. $US/ask.sh
ESPPORT=$($US/ftdi_finder.py)
if ask "Set ESPPORT to $ESPPORT?" Y; then
  export ESPPORT
  echo Using FTDI port at $ESPPORT
fi
if ask "Set ESPBAUD to 460800?" Y; then
  export ESPBAUD=460800
fi

unset -f ask
